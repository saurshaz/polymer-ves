
const handlers = require('../handlers.js')
const store = require('../store.js')
const PubSub = require('../pubsub.js')

module.exports = {
  // init method is a special one which can initialize
  // the mixin when it's loaded to the tag and is not
  // accessible from the tag its mixed in
  init: function (self) {
    // this.on('updated', function () { console.log('Updated!') })

    self.state = store.init()
    self._ = self.state
    
    let identifier = self.identifier
    
    self.props = self.props || {}
    self.props._ = self.props._ || {}

    self._.navTo = function (targetUri, container) {
      this.nextView = container
      if (targetUri.indexOf('target') !== -1) {
        let extraParams = {domain: '', page: '', view: '', target: '', fragment: ''}
        var replaced = targetUri.slice(1)
        var arr = replaced.split('&')
        for (var i = arr.length - 1; i >= 0; i--) {
          if (arr[i].split('=') && arr[i].split('=').length === 2) {
            var keyValArr = arr[i].split('=')
            extraParams[keyValArr[0]] = keyValArr[1] || ''
          }
        }
        let options = {
          domain: extraParams.domain,
          page: extraParams.page
        }

        // @todo :: polymer specific code belo
        self.importHref(['/src/polymer/common/' + extraParams.fragment + '/' + extraParams.fragment + '.html'], function(e) {
          let newElement = document.createElement(extraParams.fragment);
          let parentNode = document.querySelector(extraParams.target);
          let c = Polymer.dom(parentNode)
          c.innerHTML = ''
          c.appendChild(newElement)
          
        })
      } else {
        document.location.href = targetUri
      }
    }

    // / setup all events
    // PubSub.publish('setup_all_events', {context: self})
    function ready_func(self){
      // todo : try to get this stores from init param only
      self.stores.map((item, i) => {
        //  todo :: validation needed shall be fetched from module name
        // debugger
        PubSub.subscribe(item + '_updated', (data) => {
          for (let i in self.validationform) {
            if (data.key === self.validationform[i] && data.val.validated && data.val.validated === true) {
              PubSub.publish(identifier + '_setup_events', {context: self})
            } else if (data.key === self.validationform[i] && data.val.validated && data.val.validated !== true) {
              PubSub.publish(identifier + '_destroy_events', {context: self})
            }
          }

          console.debug(' update data >>> ', data)
          self['_'][data.module][data.key] = data.val

          if(self.set && typeof self.set === 'function')
            self.set('_.'+data.module+'.'+data.key, self.state[data.module][data.key])
            
          if(self.update) 
            self.update()
        })
      })

      if (identifier && handlers[identifier] && handlers[identifier].onmount && (typeof handlers[identifier].onmount === 'function')) {
        handlers[identifier].onmount.call(self, {page: identifier, domain: self.opts.domain}, store, null, null)
      }
      
      // add handler functions as properties to functions
      if(handlers[identifier] && Object.keys(handlers[identifier]) && self.props._){
        Object.keys(handlers[identifier] && handlers[identifier]).forEach((func, i) => {
          // @todo :: get the page and domain in abetter manner, this is not correct
          self.props._[func] = handlers[identifier][func].bind(this,{page: identifier, domain: identifier}, store, null, null)
        })

        if(self.update) 
          self.update()
      }
    }
    
    if(self.ready && typeof self.ready === "function"){
      self.ready = ready_func(self)
    }else if(self.on){
      self.on('mount',ready_func)
    }
    
    self.validate = () => {
      let identifier = this.opts.dataIs
      if (identifier && handlers[identifier] && handlers[identifier].validate && (typeof handlers[identifier].validate === 'function')) {
        handlers[identifier].validate.call(self, {page: identifier, domain: self.opts.domain}, store, null, null)
      }
    }
  }
}
