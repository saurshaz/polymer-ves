import StoreWatcher from '../../../event-manager/utils/storex.js'
// import {Routerx as *} from '../../../event-manager/utils/routerx.js'
Polymer({
      is: 'test-navi',

      ready() {
        let self = this
        // for validations
        //  - add a validationform
        //  - add a validate function in the handlers['<tag-name>'] section to set result
        //                into state.<store>.<validationform>.validated property
        self.identifier = 'counter'
        // stores to watch
        self.stores = ['counter']
        // validation form . this will have validated field
        self.validationform = []

        StoreWatcher.init(self)

        // Authx.init(self)

        // RouterX.init(self, (target, fragment, options) => {
        //   // render imperatively
        //   render(target, document.querySelector(fragment), options)
        // })

        // _visibleChanged: function(visible) {
        //   if (visible) {
        //     this.fire('change-section', { title: 'Home' });
        //   }
        // }
      },

      to(e){
        if (e.target.nodeName === 'A' && e.target.classList.contains('__trigger')){
          // @todo :: rm hardcoding
          this._.navTo(e.target.getAttribute('data-link'),'test',e.target.parentElement.parentElement.parentElement.querySelector('section#content-area'))
        }
      }
})
    