Polymer({
	is:'app-browser',
  properties : {

  },
	ready(){
		let self = this
	    self.mode = ''
	    self.set('selected_app','')
	    self.set('selected_app_name','')
	    self.set('created_app','')
	    self.set('created_app_name','')

  },

    attached() {
	    // TODO :: GET THESE DYNAMICALLY AND MAKE THEM SECURE
	    let user_id = 'saurshaz@gmail.com'
	    let appler_ac = 'saurshaz@gmail.com'
      //debugger;
      let self = this
      self.root = self.root || self

      self.$.createAppBtn.style.display = 'block'
      self.$.existingAppBtn.style.display = 'block'
      self.$.createAppDiv.style.display = 'none'


      // test call to fyler
      let fyler_config = {
        'commands': [{
          'what': 'fileio',
          'handler': 'getMatchingFiles',
          'listingpath': '/appler/' + user_id,
          'group_by_column': 'metadata.project_id',
          'user': user_id,
          'token': appler_ac
        }],
        'config': {
          'where': 'server',
          'how': 'sync',
          'lookat': 'context'
        }
      }

        // Fyler.run(fyler_config, function (err, results) {

        //   self.app_items = (results && results[0] && results[0][0]) || [{app_name:'rioter-one',app_id:'1'},{app_name:'rioter-two',app_id:'2'},{app_name:'rioter-three',app_id:'3'}]
        //   self.update()

        //    $('#app-selector').dropdown({
        //     inDuration: 300,
        //     outDuration: 225,
        //     constrain_width: false, // Does not change width of dropdown to that of the activator
        //     hover: true, // Activate on hover
        //     gutter: 0, // Spacing from edge
        //     belowOrigin: false, // Displays dropdown below the button
        //     alignment: 'left' // Displays dropdown with edge aligned to the left of button
        //   });


        // })

      // var url = function(user_id) {
      //   return 'user/'+user_id+'/public/projects';
      // }

      // var _setProjects = function(data) {
      //     //data = data.map(function(item,i){ return {_id : item[0]._id} })
      //     self.app_items = data
      //     self.noApp = (!self.app_items || (self.app_items.length === 0 ))
      //     if(!self.noApp)
      //       self.app_items.unshift({_id:'Select an app'})
      //     self.update()
      // }

      // var jsonize = function(data) {
      //     return data[0]
      // }

      // var _initSelectIfNeeded = function(data){
      //   if(!self.noApp){
      //     let select = self.root.querySelector('#existingAppBtn')
      //     let options = {
      //       enablePagination: false,
      //       removeFirstOptionFromSearch: true,
      //       useFirstOptionTextAsPlaceholder: true,
      //       placeholderText: self.selected_app_name,
      //       noResultsMessage: self.selected_app_name,
      //       onchange: function() {
      //         console.log('You selected the ' + this.text + ' model.')
      //       }
      //     }
      //     let barq = new Barq(select, options).init()
      //   }
      //   self.update()
      // }



      // var updateState = R.compose(_initSelectIfNeeded,_setProjects,jsonize)
      // var getDataApp = R.compose(Utils.getJSON(updateState), url)





    },
    processAppSelect(e){
      let target =  e.target
      self.selected_app = target.getAttribute('data-appid')
      self.selected_app_name = target.innerHTML
      self.created_app = ''
      self.created_app_name = ''
      self.$.appMessageDiv.style.display = 'block'
      //self.root.querySelector('#createAppBtn').style.display ='none'
      // self.update()
    },

    processAppCreate(e){
      let target =  e.target
      self.$.createAppDiv.style.display = 'block'
      self.$.appMessageDiv.style.display ='none'
      self.$.existingAppBtn.style.display = 'none'
      // self.update()
    },


    submitAppCreate(e){
      let target =  e.target
      self.mode = 'created'
      self.created_app_name = self.$.createAppInput.value
      self.created_app ='4'
      self.selected_app = ''
      self.selected_app_name = ''
      self.app_items.push({app_name:self.$.createAppInput.value, app_id:'4'})
      // TODO :: add logic for above hardcoding removal

      self.$.appMessageDiv.style.display = 'block'
      self.$.createAppDiv.style.display = 'none'
      //self.root.querySelector('#createAppBtn').style.display ='block'
      self.$.existingAppBtn.style.display = 'block'
      //debugger
    },

    resetAppCreate(e){
      let target =  e.target
      self.mode = ''
      self.created_app_name = ''
      self.created_app =''
      self.selected_app = ''
      self.selected_app_name = ''
      self.$.appMessageDiv.style.display = 'block'
      self.$.createAppDiv.style.display = 'none'
     // self.root.querySelector('#createAppBtn').style.display ='block'
      self.$.existingAppBtn.style.display = 'block'
      //debugger
    },

    processAppSelection(e){
      let target =  e.target
      self.mode = 'selected'
      self.selected_app =target.getAttribute('data-appid')
      self.selected_app_name = target.getAttribute('data-app_name')
      self.created_app_name = ''
      self.created_app =''
      self.root.querySelector('#createAppDiv').style.display = 'block'
      self.root.querySelector('#appMessageDiv').style.display ='block'
      self.root.querySelector('#existingAppBtn').style.display = 'none'
      // self.update()
    },

    processAppDeletion(e){
      let target =  e.target
      self.selected_app =target.getAttribute('data-appid')
      self.selected_app_name = target.innerHTML
      self.created_app_name = ''
      self.created_app =''
      self.$.createAppDiv.style.display = 'block'
      //self.root.querySelector('#createAppBtn').style.display ='none'
      self.$.existingAppBtn.style.display = 'none'
      // self.update()
    },


    deleteApp(e){
      let target =  e.target
      self[self.mode +'_app'] = target.getAttribute('data-app_id')
      self[self.mode +'_app_name'] = target.getAttribute('data-app_name')
      // todo :: add logic to actually remove app
      debugger
    },

    goToApp(e){
      let target =  e.target
      self[self.mode +'_app'] = target.getAttribute('data-app_id')
      self[self.mode +'_app_name'] = target.getAttribute('data-app_name')
      // todo :: add logic to actually navigate to that App
      debugger
    }

})